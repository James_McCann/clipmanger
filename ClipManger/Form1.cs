﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using UIAdditions;
using Gma.System.MouseKeyHook;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

namespace ClipManger
{
    public partial class Form1 : Form
    {
        public Notification Alert;

        private IKeyboardMouseEvents GlobalHook;

        public List<Clipdata> temp = new List<Clipdata>();

        public List<string> Ignore = new List<string>();

        private int _clipcount;

        #region Imports

        public const int WM_NCLBUTTONDOWN = 0xA1;

        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        #endregion
       
        public Form1()
        {
            InitializeComponent();
            Alert = new Notification("Test");
            GlobalHook = Hook.GlobalEvents();
            AssignKeyDownEvent();
            SetupList();
        }

        public void SetupList()
        {
            if (File.Exists("temp.bin"))
            {
                List<Clipdata> data = DataManger.LoadData<List<Clipdata>>("temp.bin");
                foreach (Clipdata d in data)
                {
                    temp.Add(new Clipdata(d.date, d.rawclip));
                }

                foreach (Clipdata d in temp)
                {
                    listBox1.Items.Add($"{d.rawclip}");
                }
                _clipcount = data.Count;
                data.Clear();
                Alert.Popup($"Loaded {_clipcount} Clipdata Items", Notification.Type.Success);
            }
            else
            {
                File.Create("temp.bin").Dispose();
            }

            if (File.Exists("Ignore.bin"))
            {
                List<string> ignoreTemp = DataManger.LoadData<List<string>>("Ignore.bin");
                foreach (string d in ignoreTemp)
                {
                    Ignore.Add(d);
                }
                ignoreTemp.Clear();
            }
            else
            {
                File.Create("Ignore.bin").Dispose();
            }
        }

        private void AssignKeyDownEvent()
        {
            GlobalHook.KeyDown += GlobalHook_KeyDown;
        }

        private void DeAssignKeyDownEvent()
        {
            GlobalHook.KeyDown -= GlobalHook_KeyDown;
        }

        private async void GlobalHook_KeyDown(object sender, KeyEventArgs e)
        {
            //MessageBox.Show(e.KeyData.ToString());
            if (e.Control && e.KeyCode == Keys.C)
            {
                await Task.Delay(50);
                HandleClipbordText();
            }

            if (e.Control && e.KeyCode == Keys.Up)
            {
                cyclethroughClipbord(direction.Up);
            }

            if (e.Control && e.KeyCode == Keys.Down)
            {
                cyclethroughClipbord(direction.Down);
            }

            if (e.Control && e.KeyCode == Keys.D)
            {
                //Start(Clipboard.GetText());
            }

            if (e.Control && e.KeyCode == Keys.C && e.Shift)
            {
                Process.Start("Cmd.exe");
            }
        }

        /// <summary>
        /// MouseDown callback used so we can move our main form window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int num = listBox1.SelectedIndex;
            string Text = listBox1.SelectedItem.ToString();
            if (Regex.IsMatch(Text, @"^[a-zA-Z]:\\"))
            {
                if (File.Exists(Text) || Directory.Exists(Text))
                {
                    StringCollection returnList = new StringCollection();
                    returnList.Add(Text);
                    Clipboard.SetFileDropList(returnList);
                    Alert.Popup($"{Text} {GetFileSize(Text)}", Notification.Type.Info);
                }
                else
                {
                    listBox1.Items.RemoveAt(num); temp.RemoveAt(num);
                    listBox1.SelectedItem = 0;
                }
            }
            else
            {
                Clipboard.SetText(Text);
                Alert.Popup(Text, Notification.Type.Info);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (_clipcount != temp.Count)
            {
                DataManger.SaveData(temp, "temp.bin");
                DataManger.SaveData(Ignore, "Ignore.bin");
                _clipcount = temp.Count;
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int num = listBox1.SelectedIndex;
            if(num != -1)
            {
                Ignore.Add(listBox1.SelectedItem.ToString());
                listBox1.Items.RemoveAt(num); temp.RemoveAt(num);
                listBox1.SelectedIndex = num - 1;
                listBox1.SelectedItem = num - 1;
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listBox1.SelectedItem != null)
            {
                string Temp = listBox1.SelectedItem.ToString();
                if (File.Exists(Temp) || Directory.Exists(Temp))
                {
                    foreach (Clipdata d in temp)
                    {
                        if (listBox1.SelectedItem != null)
                        {
                            if (d.rawclip.Equals(listBox1.SelectedItem.ToString()))
                            {
                                if (Regex.IsMatch(listBox1.SelectedItem.ToString(), @"^[a-zA-Z]:\\"))
                                {
                                    int num = listBox1.SelectedIndex;
                                    if (File.Exists(listBox1.SelectedItem.ToString()) || Directory.Exists(listBox1.SelectedItem.ToString()))
                                    {
                                        InfoLabel.Text = ($"{d.date} | {GetFileSize(d.rawclip)}");
                                    }
                                    else
                                    {
                                        Ignore.Add(listBox1.SelectedItem.ToString());
                                        listBox1.Items.RemoveAt(num); temp.RemoveAt(num);
                                        listBox1.SelectedIndex = num - 1;
                                        listBox1.SelectedItem = num - 1;
                                    }
                                }
                                else
                                {
                                    InfoLabel.Text = ($"{d.date}");
                                }
                            }
                        }
                    }
                }
                if (Regex.IsMatch(listBox1.SelectedItem.ToString(), @"^[a-zA-Z]"))
                {
                    Clipboard.SetText(listBox1.SelectedItem.ToString());
                }
                else
                {
                    int num = listBox1.SelectedIndex;
                    Ignore.Add(listBox1.SelectedItem.ToString());
                    listBox1.Items.RemoveAt(num); temp.RemoveAt(num);
                    listBox1.SelectedIndex = num - 1;
                    listBox1.SelectedItem = num - 1;
                }
            }
            else
            {

            }
        }

        private void gotoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem.ToString().StartsWith("https://") || listBox1.SelectedItem.ToString().StartsWith("www."))
            {
                Process.Start(listBox1.SelectedItem.ToString());
            }
            else if (Regex.IsMatch(listBox1.SelectedItem.ToString(), @"^[a-zA-Z]:\\"))
            {
                if (File.Exists(listBox1.SelectedItem.ToString()) || Directory.Exists(listBox1.SelectedItem.ToString()))
                {
                    Process.Start("explorer.exe", $"/select, {listBox1.SelectedItem.ToString()}");
                }
                else
                {
                    listBox1.Items.RemoveAt(listBox1.SelectedIndex); temp.RemoveAt(listBox1.SelectedIndex);
                    listBox1.SelectedItem = 0;
                }
            }
        }

        private void fullDeleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string Temp = listBox1.SelectedItem.ToString();
            if(File.Exists(Temp) || Directory.Exists(Temp))
            {
                int num = listBox1.SelectedIndex;
                Ignore.Add(Temp);
                listBox1.Items.RemoveAt(num);
                temp.RemoveAt(num);
                listBox1.SelectedIndex = num - 1;
                FileAttributes AA = File.GetAttributes(Temp);
                if ((AA & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    Directory.Delete(Temp);
                }
                else
                {
                    File.Delete(Temp);
                }
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            gotoToolStripMenuItem.Text = "Goto";
            fullDeleteToolStripMenuItem.Visible = false;
            gotoToolStripMenuItem.Visible  = false;
            deleteToolStripMenuItem.Visible = false;
            copyLinkToolStripMenuItem.Visible = false;
            if(listBox1.SelectedIndex != -1)
            {
                deleteToolStripMenuItem.Visible = true;
                if (Regex.IsMatch(listBox1.SelectedItem.ToString(), @"^[a-zA-Z]:\\"))
                {
                    fullDeleteToolStripMenuItem.Visible = true;
                    gotoToolStripMenuItem.Visible = true;
                    gotoToolStripMenuItem.Text = "Show in Folder";
                }
                if (listBox1.SelectedItem.ToString().StartsWith("https://") || listBox1.SelectedItem.ToString().StartsWith("www."))
                {
                    gotoToolStripMenuItem.Visible = true;
                }
                if (listBox1.SelectedItem.ToString().Contains("https://") || listBox1.SelectedItem.ToString().Contains("www."))
                {
                    copyLinkToolStripMenuItem.Visible = true;
                }

                if (listBox1.SelectedItem.ToString().Contains("https://") || listBox1.SelectedItem.ToString().Contains("www.") && listBox1.SelectedItem.ToString().Contains("Soundcloud"))
                {
                   
                }
            }
            else
            {
                contextMenuStrip1.Close();
            }
        }

        StringCollection LastBatchCopy;
        private void Update_Tick(object sender, EventArgs e)
        {
            string text; 
            if (Clipboard.ContainsFileDropList()) { LastBatchCopy = Clipboard.GetFileDropList(); }
            if ((text = Clipboard.GetText()).Length != 0 && !listBox1.Items.Contains(text) && !String.IsNullOrWhiteSpace(text) && !Ignore.Contains(text))
            {
                HandleClipbordText();
            }
            if(Clipboard.ContainsFileDropList())
            {
                var copy = Clipboard.GetFileDropList();
                if (copy.Count > 1)
                {
                    if (!copy[1].Equals(LastBatchCopy[1]))
                    {
                        HandleClipbordText();
                    }
                }  
                foreach (string s in copy)
                {
                   if (!listBox1.Items.Contains(s) && !Ignore.Contains(s))
                    {
                        HandleClipbordText();
                    }
                }
            }
        }

        private void copyLinkToolStripMenuItem_Click(object sender, EventArgs e)
        {
           if (listBox1.SelectedIndex != -1)
            {
                var linkParser = new Regex(@"\b(?:https?://|www\.)\S+\b", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                foreach (Match m in linkParser.Matches(listBox1.SelectedItem.ToString()))
                {
                    Clipboard.SetText(m.Value);
                    Ignore.Add(m.Value);
                }
            }
        }

        public enum direction { Down,Up };
        public void cyclethroughClipbord(direction direction)
        {
            int num = listBox1.SelectedIndex;

            if (direction == direction.Up)
            {
                if (num != -1) { num--; }
                if (num != -1)
                {
                    listBox1.SelectedIndex = num;
                    if (Regex.IsMatch(listBox1.SelectedItem.ToString(), @"^[a-zA-Z]:\\"))
                    {
                        if (File.Exists(listBox1.SelectedItem.ToString()) || Directory.Exists(listBox1.SelectedItem.ToString()))
                        {
                            StringCollection returnList = new StringCollection();
                            returnList.Add(listBox1.SelectedItem.ToString());
                            Clipboard.SetFileDropList(returnList);
                            Alert.Popup($"{Path.GetFileName(listBox1.SelectedItem.ToString())} {GetFileSize(listBox1.SelectedItem.ToString())}", Notification.Type.Info);
                        }
                        else
                        {
                            listBox1.Items.RemoveAt(num); temp.RemoveAt(num);
                            listBox1.SelectedItem = 0;
                        }
                    }
                    else
                    {
                        Clipboard.SetText(listBox1.SelectedItem.ToString());
                        Alert.Popup(Clipboard.GetText(), Notification.Type.Info);
                    }
                }
            }
            if (direction == direction.Down)
            {
                num++;
                if (num <= listBox1.Items.Count - 1)
                {
                    listBox1.SelectedIndex = num;
                    if (Regex.IsMatch(listBox1.SelectedItem.ToString(), @"^[a-zA-Z]:\\"))
                    {
                        if (File.Exists(listBox1.SelectedItem.ToString()) || Directory.Exists(listBox1.SelectedItem.ToString()))
                        {
                            StringCollection returnList = new StringCollection();
                            returnList.Add(listBox1.SelectedItem.ToString());
                            Clipboard.SetFileDropList(returnList);
                            Alert.Popup($"{Path.GetFileName(listBox1.SelectedItem.ToString())} {GetFileSize(listBox1.SelectedItem.ToString())}", Notification.Type.Info);
                        }
                        else
                        {
                            listBox1.Items.RemoveAt(num); temp.RemoveAt(num);
                            listBox1.SelectedItem = 0;
                        }
                    }
                    else
                    {
                        Clipboard.SetText(listBox1.SelectedItem.ToString());
                        Alert.Popup(Clipboard.GetText(), Notification.Type.Info);
                    }
                }
            }
        }

        public void HandleClipbordText()
        {
            string text;
            if ((text = Clipboard.GetText()).Length != 0 && !listBox1.Items.Contains(text) && !string.IsNullOrWhiteSpace(text))
            {
                listBox1.Items.Add(text);
                temp.Add(new Clipdata(DateTime.Now, text));
                Alert.Popup(text, Notification.Type.Info);
            }
            else
            {
                var copy = Clipboard.GetFileDropList();

                if (copy.Count > 1)
                {
                    foreach (string s in copy)
                    {
                        listBox1.Items.Add(s);
                        temp.Add(new Clipdata(DateTime.Now, s));
                        Alert.Popup($"Copied { copy.Count } Files | { GetFileSize(Clipboard.GetFileDropList()) }", Notification.Type.Info);
                    }
                }
                else if (Clipboard.ContainsFileDropList())
                {
                    foreach (string s in copy)
                    {
                        listBox1.Items.Add(s);
                        temp.Add(new Clipdata(DateTime.Now, s));
                        Alert.Popup($"{Path.GetFileName(s)} | { GetFileSize(s) }", Notification.Type.Info);
                    }
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DataManger.SaveData(temp, "temp.bin");
            DataManger.SaveData(Ignore, "Ignore.bin");
        }

        private void bunifuCustomLabel1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private string GetFileSize(string Path)
        {
            string[] sizes = { "B", "KB", "MB", "GB", "TB" };
            int order = 0;
            if (File.Exists(Path))
            {
                double len = new FileInfo(Path).Length;
                while (len >= 1024 && order < sizes.Length - 1)
                {
                    order++;
                    len = len / 1024;
                }
                return String.Format("{0:0.##} {1}", len, sizes[order]);
            }
            if(Directory.Exists(Path))
            {
                string[] Files = Directory.GetFiles(Path, "*.*",SearchOption.AllDirectories);
                long totalFileSize = 0;
                foreach (string fileName in Files)
                {
                    FileInfo info = new FileInfo(fileName);
                    totalFileSize = totalFileSize + info.Length;
                }
                while (totalFileSize >= 1024 && order < sizes.Length - 1)
                {
                    order++;
                    totalFileSize = totalFileSize / 1024;
                }
                return String.Format("{0:0.##} {1}", totalFileSize, sizes[order]);
            }
            return "Na";
        }

        private string GetFileSize(StringCollection Files)
        {
            string[] sizes = { "B", "KB", "MB", "GB", "TB" };
            int order = 0;
            long totalFileSize = 0;
            foreach (string Fpath in Files)
            {
                FileAttributes A = File.GetAttributes(Fpath);
                if ((A & FileAttributes.Directory) != FileAttributes.Directory)
                {
                    FileInfo info2 = new FileInfo(Fpath);
                    totalFileSize += info2.Length;
                }
                else
                {
                    string[] FilesinDir = Directory.GetFiles(Fpath, "*.*", SearchOption.AllDirectories);
                    foreach (string fileName in FilesinDir)
                    {
                        FileInfo info = new FileInfo(fileName);
                        totalFileSize += info.Length;
                    }
                }
            }
            while (totalFileSize >= 1024 && order < sizes.Length - 1)
            {
                order++;
                totalFileSize = totalFileSize / 1024;
            }
            return String.Format("{0:0.##} {1}", totalFileSize, sizes[order]);
        }

        private void bunifuCustomLabel2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {

        }
    }
}
