﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace ClipManger
{
    [ProtoContract]
    public class Clipdata
    {
        [ProtoMember(1)]
        //The date when the clip was saved.
        public DateTime date { get; set; }

        [ProtoMember(2)]
        //The clip as type string.
        public string rawclip { get; set; }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public Clipdata()
        {

        }

        /// <summary>
        /// Data Constructor
        /// </summary>
        /// <param name="dt"><summary>Date of clip</summary></param>
        /// <param name="clip"><summary>Raw string of clip</summary></param>
        public Clipdata(DateTime dt, string clip)
        {
            date = dt;
            rawclip = clip;
        }
    }
}
